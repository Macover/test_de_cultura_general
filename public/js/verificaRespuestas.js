

function verificaRespuestas(){

    var totalPreguntas = 3;
    var puntosUsuario = 0;

    var formulario = document.forms["formularioUsuario"]; // <-- obtiene los datos del formulario con el name de "formularioUsuario"
    var respuestasCorrectas = ["a","c","d","a","b","d","c","b","a","d"];
    // respuestasCorrectas contiene las respuestas correctas a cada pregunta del formulario

    // recorrera el formulario para ver si respondio o se es correcta la respuesta
    for(var i =1; i<=totalPreguntas; i++ ){
        // si no contesto enviar alerta para que responda
        if (formulario["pregunta" + i].value === null || formulario["pregunta" + i].value === ""){
            alert("Por favor responda la pregunta " + i);
            return false; // <-- termina el ciclo si no respondio nada
        }else{ //verifica si la respuesta es corrcta y compara la respuesta del usuario con nuestro arreglo
            // de respuestasCorrectas .
            if (formulario["pregunta" + i].value === respuestasCorrectas[i-1]){
                puntosUsuario++;
            }
        }
    }
    var resultado = document.getElementById("resultadoFormulario");
    resultado.innerHTML = '<h3> Conseguiste un puntaje de <span name="puntosUsuario">'+ puntosUsuario +'</span>/<span>'+ totalPreguntas +'</span>';

    // alert("Conseguiste un puntaje de "+puntosUsuario+"/"+totalPreguntas);

    /*
    numeros de ifs que evualuaran al usuario con un mensaje en pantalla.
    if (puntosUsuario === 10){

    }
    */
    return false;

}