<?php

namespace test;

class Usuario extends Conexion
{

    public $nombreUsuario;
    public $correo;
    public $edad;
    public $sexo;

    public function __construct()
    {
        parent::__construct();
    }

    function registrarNuevoUsuarioBD(){

        $pre = mysqli_prepare($this->con,"INSERT INTO `usuarios`(`nombre_usuario`, `correo`, `edad`, `genero`) VALUES (?,?,?,?)");
        $pre->bind_param("ssis",$this->nombreUsuario, $this->correo, $this->edad, $this->sexo);
        $pre->execute();

    }


    static function consultaIdUsuario($correoForm){

        $conexion = new Conexion();
        $pre = mysqli_prepare($conexion->con,"SELECT * FROM usuarios WHERE correo = ?");
        $pre->bind_param("s",$correoForm);
        $pre->execute();
        $resultado = $pre->get_result();
        return $resultado->fetch_object();

    }

    static function consultaDatos($id){

        $conexion = new Conexion();
        $pre = mysqli_prepare($conexion->con,"SELECT * FROM usuarios WHERE id_usuario = ?");
        $pre->bind_param("i",$id);
        $pre->execute();
        $resultado = $pre->get_result();
        return $resultado->fetch_object();

    }



}