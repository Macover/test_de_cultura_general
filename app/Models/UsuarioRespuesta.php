<?php


namespace test;


class UsuarioRespuesta extends Conexion
{

    public $idUsuario;
    public $numeroPregunta;
    public $usuarioRespuesta;


    public function __construct()
    {
        parent::__construct();
    }

    function subirRespuestasBD(){

        $pre = mysqli_prepare($this->con,"INSERT INTO `usuario_respuesta`(`id_usuario`, `numero_pregunta`, `usuario_respuesta`) VALUES (?,?,?)");
        $pre->bind_param("iss",$this->idUsuario, $this->numeroPregunta, $this->usuarioRespuesta);
        $pre->execute();

    }


}