<?php


namespace test;


class UsuarioTest extends Conexion
{
    public $idUsuario;
    public $genero;
    public $dia;
    public $numeroRespuestasCorrectas;


    public function __construct()
    {
        parent::__construct();
    }

    function subirRespuestasBD(){

        $pre = mysqli_prepare($this->con,"INSERT INTO `usuario_test`(`id_usuario`, `genero`, `dia`, `numero_respuestas_correctas`) VALUES (?,?,?,?)");
        $pre->bind_param("isii",$this->idUsuario, $this->genero, $this->dia, $this->numeroRespuestasCorrectas);
        $pre->execute();

    }

    static function consultaNumRespuestas($id){

        $conexion = new Conexion();
        $pre = mysqli_prepare($conexion->con,"SELECT `numero_respuestas_correctas` FROM `usuario_test` WHERE `id_usuario` = ?");
        $pre->bind_param("i",$id);
        $pre->execute();
        $resultado = $pre->get_result();
        return $resultado->fetch_object();

    }
    static function consultarPromedios($inicio,$limite){

        $me = new Conexion();
        $pre = mysqli_prepare($me->con,"SELECT * FROM `usuario_test` WHERE dia > ? AND dia <= ?");
        $pre->bind_param("ii",$inicio, $limite);
        $pre->execute();
        $res = $pre->get_result();
        while($y=mysqli_fetch_assoc($res)){
            $resultadosTabla[]=$y;
        }
        //var_dump($resultadosTabla);

        $puntosHombre = 0;
        $puntosMujer = 0;

        for($i=0; $i<count($resultadosTabla); $i++){

            if ($resultadosTabla[$i]["genero"] == "Hombre"){

            $puntosHombre= $puntosHombre + $resultadosTabla[$i]["numero_respuestas_correctas"];

            }elseif ($resultadosTabla[$i]["genero"] == "Mujer"){

            $puntosMujer= $puntosMujer + $resultadosTabla[$i]["numero_respuestas_correctas"];

            }

        }
        $promediosHombres = $puntosHombre/=count($resultadosTabla);
        $promediosMujeres = $puntosMujer/=count($resultadosTabla);
        $promedios = [];

        $promedios[0] = $promediosHombres;
        $promedios[1] = $promediosMujeres;


        return $promedios;


    }




}