<?php


namespace test;


class UsuarioRespuestas extends Conexion
{

    public $idUsuario;
    public $numeroPregunta;
    public $usuarioRespuesta;
    public $numeroRespuestasCorrectas;

    public function __construct()
    {
        parent::__construct();
    }


    function subirRespuestasBD(){

        $pre = mysqli_prepare($this->con,"INSERT INTO `usuario_pregunta`(`id_usuario`, `numero_pregunta`, `usuario_respuesta`, `numero_respuestas_correctas`) VALUES (?,?,?,?)");
        $pre->bind_param("issi",$this->idUsuario, $this->numeroPregunta, $this->usuarioRespuesta, $this->numeroRespuestasCorrectas);
        $pre->execute();

    }


}