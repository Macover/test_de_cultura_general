<?php

require 'app/Models/Conexion.php';
require 'app/Models/Usuario.php';
require 'app/Models/UsuarioRespuesta.php';
require 'app/Models/UsuarioTest.php';

use test\Usuario;
use test\UsuarioRespuesta;
use test\UsuarioTest;
use test\Conexion;


class UsuarioController
{



    public function __construct()
    {
        if($_GET["action"] == "preguntasUsuarioForm" || $_GET["action"] == "promedioSemana" ||
        $_GET["action"] == "verificarPreguntasUsuario" || $_GET["action"] == "muestraComparacion" ||
        $_GET["action"] == "comparacion"){
            if(!isset($_SESSION["correoSe"])){
                //no sea iniciado sesion y redireccionara a la vista login
                echo "No has iniciado sesion";
                header("location:/test/index.php?controller=Usuario&action=inicio");
            }
        }
    }

    function inicio(){

        require "app/Views/formularioUsuario.php";
        $this->verificarDatosUsuario();

    }

    function verificarDatosUsuario(){

        // metodo que sube los datos del usuario a la bd


        if(!isset($_POST["nombreUsuario"]) || !isset($_POST["correo"]) ||
            !isset($_POST["edad"]) || !isset($_POST["genero"])){


        }else{
            $nuevoUsuario = new Usuario();
            $nuevoUsuario->nombreUsuario = $_POST["nombreUsuario"];
            $correo = $_POST["correo"];
            $nuevoUsuario->correo = $_POST["correo"];
            $nuevoUsuario->edad = $_POST["edad"];
            $nuevoUsuario->sexo = $_POST["genero"];
            $nuevoUsuario->registrarNuevoUsuarioBD();
            $_SESSION["correoSe"] = $correo;
            $this->preguntasUsuarioForm();
        }

    }

    function preguntasUsuarioForm(){

        require "app/Views/formularioPreguntas.php";
        $this->verificarPreguntasUsuario();

    }
    function verificarPreguntasUsuario(){

        //metodo que enviara las respuestas seleccionadas por el usuario a la BD


        if(!isset($_POST["pregunta1"]) || !isset($_POST["pregunta2"]) || !isset($_POST["pregunta3"]) ||
            !isset($_POST["pregunta4"]) || !isset($_POST["pregunta5"]) || !isset($_POST["pregunta6"]) ||
            !isset($_POST["pregunta7"]) || !isset($_POST["pregunta8"]) || !isset($_POST["pregunta9"]) ||
            !isset($_POST["pregunta10"])){



        }else{

            $respuestasUsuario = array();
            $respuestasCorrestas = array("b","a","b","c","b","b","a","c","a","a",);
            $puntosUsuario = 0;

            for ($i = 1; $i<=10; $i++){
                $respuestasUsuario[$i] = $_POST["pregunta".$i];
            }

            for ($i = 1; $i<=10; $i++){
                if ($respuestasUsuario[$i] == $respuestasCorrestas[$i-1]){
                    $puntosUsuario++;
                }
            }

            // enviar respuestas a la bd asd a

            $enviarRespuestas = new UsuarioRespuesta();

            // PEDIR ID USUARIO

            $idUser = Usuario::consultaIdUsuario($_SESSION["correoSe"]);


            for ($i = 1; $i<=10; $i++){


                $enviarRespuestas->idUsuario = $idUser->id_usuario;
                $enviarRespuestas->numeroPregunta = "pregunta ".$i;
                $enviarRespuestas->usuarioRespuesta = $respuestasUsuario[$i];
                $enviarRespuestas->subirRespuestasBD(); // metodo que registra las respuestas del usuario en la bd.

            }
            //modificar datos para el formulario
            //enviar id usuario
            $testUsuario = new UsuarioTest();
            $testUsuario->idUsuario = $idUser->id_usuario;
            $testUsuario->genero = $idUser->genero;
            $testUsuario->dia = $_POST["dia"];
            $testUsuario->numeroRespuestasCorrectas = $puntosUsuario;
            $testUsuario->subirRespuestasBD();

            echo '<link rel="stylesheet" href="public/css/estilosFormularioUsuario.css"><form method="post" action="index.php?controller=Usuario&action=comparacion">
               <input type="submit" value="Siguiente"></input><br>
              </form>';


        }

    }

    function comparacion(){

        require "app/Views/vistaComparar.php";
        $this->muestraComparacion();


    }

    function muestraComparacion(){

        $idUser = Usuario::consultaIdUsuario($_SESSION["correoSe"]);
        $numRespuestas = UsuarioTest::consultaNumRespuestas($idUser->id_usuario);

        //datos del Usuario recien registrado

        echo '<link rel="stylesheet" href="public/css/estilosFormularioUsuario.css"><table class="default" align="center" cellspacing="2" cellpadding="2" border="1" bgcolor="white" style="font-size: 20px;">
                <br><br><br><br><br>
               <tr>
               <td colspan="5" align="center"> <font color="black">Comparación Usuarios</font>
               </td>
               </tr>
               
               <td align="center">Id del usuario</td>
               <td align="center">nombre del usuario</td>
               <td align="center">Edad</td>
               <td align="center">genero</td>
               <td align="center">respuestas correctas</td>
               </tr>
               <tr>
               <td align="center">'.$idUser->id_usuario.'</td>
               <td align="center">'.$idUser->nombre_usuario.'</td>
               <td align="center">'.$idUser->edad.'</td>
               <td align="center">'.$idUser->genero.'</td>
               <td align="center">'.$numRespuestas->numero_respuestas_correctas.'</td>
               </tr>
              </table>';

        //datos del otro Usuario

        $idRandom = rand(1,2);
        $datosOtroUsuario = Usuario::consultaDatos($idRandom);
        $numRespuestasOtroUsuario = UsuarioTest::consultaNumRespuestas($idRandom);

        echo '<link rel="stylesheet" href="public/css/estilosFormularioUsuario.css"><table class="tabla2" align="center" cellspacing="2" cellpadding="2" border="1" bgcolor="white" style="font-size: 20px;">
               
               <tr>
               <td align="center">Id del usuario</td>
               <td align="center">nombre del usuario</td>
               <td align="center">Edad</td>
               <td align="center">genero</td>
               <td align="center">respuestas correctas</td>
               </tr>
               <tr>
               <td align="center">'.$datosOtroUsuario->id_usuario.'</td>
               <td align="center">'.$datosOtroUsuario->nombre_usuario.'</td>
               <td align="center">'.$datosOtroUsuario->edad.'</td>
               <td align="center">'.$datosOtroUsuario->genero.'</td>
               <td align="center">'.$numRespuestasOtroUsuario->numero_respuestas_correctas.'</td>
               </tr>
               
              </table><br>';


        //comparacion de quien fue el mejor.

        if ($numRespuestas->numero_respuestas_correctas>$numRespuestasOtroUsuario->numero_respuestas_correctas){
            echo '<h2>'."Tu fuiste el mejor! :D".'</h2>';
        }else if($numRespuestas->numero_respuestas_correctas<$numRespuestasOtroUsuario->numero_respuestas_correctas){
            echo '<h2>'."Te ganaron :(".'</h2>';
        }
        if ($numRespuestas->numero_respuestas_correctas == $numRespuestasOtroUsuario->numero_respuestas_correctas)
            echo '<h2>'."uufff empate".'</h2>';
        }

        function promedioSemana(){

        require "app/Views/promedioSemana.php";
        echo '<h1 id="promeioH1">'."Los promedios de la semana ".$_POST["semana"]." del mes de febrero son: ".'</h1>';
        $inicio = 0;
        $limite = 0;

        if ($_POST["semana"]==1){
            //semana 1
            $inicio = 1;
            $limite = 7;
        }elseif ($_POST["semana"]==2){
            //semana 2
            $inicio = 7;
            $limite = 14;
        }elseif ($_POST["semana"]==3){
            //semana 3
            $inicio = 14;
            $limite = 21;
        }
        else {
            $inicio = 21;
            $limite = 28;
            //semana 4
        }

        $promedioMujeres = 0;
        $promedioHombres = 0;
        $promedios = [];
        $promedios = UsuarioTest::consultarPromedios($inicio,$limite);
        $promedioHombres = $promedios[0];
        $promedioMujeres = $promedios[1];
        echo '<h2>Promedio hombres: '.$promedioHombres.'</h2><br>';
        echo '<h2>Promedio mujeres: '.$promedioMujeres.'</h2><br>';

        if(isset($_SESSION["correoSe"])){
                unset($_SESSION["correoSe"]);
        }

        echo '<link rel="stylesheet" href="public/css/estilosFormularioUsuario.css"><form method="post" action="index.php?controller=Usuario&action=inicio">
               <input type="submit" value="Contestar otro"></input><br> 
              </form>';
        }

}
