<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Nombre del test</title>
    <link rel="stylesheet" href="public/css/estilosFormularioUsuario.css">
</head>
<body>

<div id="">


    <section id="comparar">

        <header>
            <h1>Tus resultados con las de otro usuario</h1>
            <p></p>
        </header>
        <form method="POST" action="index.php?controller=Usuario&action=promedioSemana">

            <label>¿Quien sabe mas? Hombres vs Mujeres</label><br>
            <label>Seleccione una semana para ver el promedio de esta.</label><br>
            <label id="selecFecha">Seleccione una semana</label>
            <select class="fecha" name="semana" size="1" required>
                <option value="" disabled="" selected="">Semana del mes</option>
                <option value="1">Semana 1</option>
                <option value="2">Semana 2</option>
                <option value="3">Semana 3</option>
                <option value="4">Semana 4</option>
            </select>

            <input id="mostrar" type="submit" value="Mostrar">

        </form>
    </section>
</div>
</body>
</html>