<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cultura General</title>
    <link rel="stylesheet" href="public/css/estilosFormularioUsuario.css">
</head>
<body>



    <section id="formulario">
        <nav>
            <h1> Ingrese sus datos para contestar el test</h1>
        </nav>
        <div id="form">

            <form method="POST" action="index.php?controller=Usuario&action=verificarDatosUsuario">
                <label>Nombre de usuario: </label><br>
                <input type="text" name="nombreUsuario" required><br>
                <label >Correo: </label><br>
                <input type="text" name="correo" required><br>
                <label >Edad: </label><br>
                <input type="text" name="edad" required><br>
                <label>Genero: </label><br>
                <input type="radio" name="genero" value="Hombre" required>Masculino<br>
                <input type="radio" name="genero" value="Mujer" required>Femenino<br><br><br>
                <input type="submit" value="¡Empieza el test!"><br>
            </form>
        </div>
    </section>

</body>
</html>