<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> Preguntas Cultura General</title>
    <link rel="stylesheet" href="public/css/estilosFormularioUsuario.css">
    </head>
<body>

    <div id="contenedor">


        <section id="formularioPreguntas">
            <header>
                <h1>Test de cultura general | Febrero!</h1><br>
                <p>Selecciona la opción que consideres correcta y
                descubre tu nivel de cultura general
                </p>
            </header>

            <div id="resultadoFormulario"></div>


            <form method="post" action="index.php?controller=Usuario&action=preguntasUsuarioForm">

                <label>Seleccione el dia de la elaboración del test</label>
                <br><label id="selecFecha">Día</label>
                <select class="fecha" name="dia" size="1" required>
                    <option value="" disabled="" selected="">Día</option>
                    <option value="1">01</option>
                    <option value="2">02</option>
                    <option value="3">03</option>
                    <option value="4">04</option>
                    <option value="5">05</option>
                    <option value="6">06</option>
                    <option value="7">07</option>
                    <option value="8">08</option>
                    <option value="9">09</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                    <option value="13">13</option>
                    <option value="14">14</option>
                    <option value="15">15</option>
                    <option value="16">16</option>
                    <option value="17">17</option>
                    <option value="18">18</option>
                    <option value="19">19</option>
                    <option value="20">20</option>
                    <option value="21">21</option>
                    <option value="22">22</option>
                    <option value="23">23</option>
                    <option value="24">24</option>
                    <option value="25">25</option>
                    <option value="26">26</option>
                    <option value="27">27</option>
                    <option value="28">28</option>
                </select>

                <h4>1.- ¿Cuál es el nombre del río más largo del mundo?</h4>

                <input type="radio" name="pregunta1" value="a" required>Río Nilo<br>
                <input type="radio" name="pregunta1" value="b" required>Río Amazonas<br>
                <input type="radio" name="pregunta1" value="c" required>Río Danubio<br>

                <h4>2.- ¿Cuál es el océano más grande del mundo?</h4>

                <input type="radio" name="pregunta2" value="a" required>Océano Pacífico<br>
                <input type="radio" name="pregunta2" value="b" required>Océano Índigo<br>
                <input type="radio" name="pregunta2" value="c" required>Océano Atlántico<br>


                <h4>3.- ¿Cuál es el país más grande del mundo?</h4>

                <input type="radio" name="pregunta3" value="a" required>China<br>
                <input type="radio" name="pregunta3" value="b" required>Rusia<br>
                <input type="radio" name="pregunta3" value="c" required>India<br>

                <h4>4.- ¿Cuál es el país que tiene forma de bota?</h4>

                <input type="radio" name="pregunta4" value="a" required>España<br>
                <input type="radio" name="pregunta4" value="b" required>Hoduras<br>
                <input type="radio" name="pregunta4" value="c" required>Italia<br>

                <h4>5.- ¿Cuál es el país más poblado de la tierra?</h4>

                <input type="radio" name="pregunta5" value="a" required>Estados Unidos<br>
                <input type="radio" name="pregunta5" value="b" required>China<br>
                <input type="radio" name="pregunta5" value="c" required>Rusia<br>

                <h4>6.- ¿Cuál es la ciudad de los rascacielos?</h4>

                <input type="radio" name="pregunta6" value="a" required>Tokio<br>
                <input type="radio" name="pregunta6" value="b" required>New York<br>
                <input type="radio" name="pregunta6" value="c" required>Hong Kong<br>

                <h4>7.- ¿En qué país se encuentra ubicada la Casa Rosada?</h4>

                <input type="radio" name="pregunta7" value="a" required>Argentina<br>
                <input type="radio" name="pregunta7" value="b" required>Chile<br>
                <input type="radio" name="pregunta7" value="c" required>México<br>

                <h4>8. ¿Cuál es la capital de Nicaragua?</h4>

                <input type="radio" name="pregunta8" value="a" required>Santiago<br>
                <input type="radio" name="pregunta8" value="b" required>Brasilia<br>
                <input type="radio" name="pregunta8" value="c" required>Managua<br>

                <h4>9.- ¿En qué país está ubicada la ciudad de Medellín?</h4>

                <input type="radio" name="pregunta9" value="a" required>Colombia<br>
                <input type="radio" name="pregunta9" value="b" required>Venezuela<br>
                <input type="radio" name="pregunta9" value="c" required>Perú<br>

                <h4>10.- ¿Cuál es la cascada más alta del mundo?</h4>

                <input type="radio" name="pregunta10" value="a" required>El salto del Ángel<br>
                <input type="radio" name="pregunta10" value="b" required>Cataratas de Iguazú<br>
                <input type="radio" name="pregunta10" value="c" required>Cataratas del Niágara<br>



            <input type="submit" id="enviar" value="Enviar!">

            </form>
        </section>
    </div>
</body>
</html>