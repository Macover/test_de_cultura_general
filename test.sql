-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 22-02-2021 a las 22:52:23
-- Versión del servidor: 10.4.17-MariaDB
-- Versión de PHP: 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `test`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(10) NOT NULL,
  `nombre_usuario` varchar(25) NOT NULL,
  `correo` varchar(35) NOT NULL,
  `edad` int(10) NOT NULL,
  `genero` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `nombre_usuario`, `correo`, `edad`, `genero`) VALUES
(1, 'Alexandra', 'alexandra@mail.com', 25, 'Mujer'),
(2, 'Ruben', 'ruben@mail.com', 21, 'Hombre'),
(3, 'Mayte', 'mayte@mail.com', 15, 'Mujer'),
(4, 'Isai', 'isai@mail.com', 21, 'Hombre'),
(5, 'Claudia', 'claudia@mail.com', 30, 'Hombre'),
(6, 'Kevin', 'kevin@mail.com', 12, 'Hombre'),
(7, 'Casandra', 'casandra@mail.com', 21, 'Mujer'),
(8, 'Alexis', 'alexis@mail.com', 26, 'Hombre'),
(9, 'Vianey', 'vianey@mail.com', 30, 'Mujer'),
(10, 'Marvin', 'marvin@mail.com', 19, 'Hombre'),
(11, 'Rosa', 'rosa@mail.com', 36, 'Mujer'),
(12, 'Jose', 'jose@mail.com', 19, 'Hombre'),
(13, 'Aide', 'aide@mail.com', 25, 'Mujer'),
(14, 'Sergio', 'sergio@mail.com', 28, 'Hombre'),
(15, 'Samantha', 'samantha@mail.com', 24, 'Mujer'),
(16, 'Erick', 'erick@mail.com', 14, 'Hombre'),
(17, 'Daniela', 'daniela@mail,com', 35, 'Mujer'),
(18, 'Joshua', 'joshua@mail.com', 20, 'Hombre'),
(19, 'Thalia', 'thalia@mail.com', 25, 'Mujer'),
(20, 'Oscar', 'oscar@mail.com', 39, 'Hombre'),
(21, 'MarÃ­a', 'maria@mail.com', 44, 'Mujer'),
(22, 'Emmanuel', 'emmanuel@mail.com', 28, 'Hombre'),
(23, 'Martina', 'martina@mail.com', 24, 'Mujer'),
(24, 'Omar', 'omar@mail.com', 20, 'Hombre'),
(25, 'Karime', 'karime@mail.com', 29, 'Mujer'),
(26, 'Alfonso', 'alfonso@mail.com', 35, 'Hombre'),
(27, 'Juan', 'juan@mail.com', 50, 'Hombre'),
(28, 'Itzel', 'itzel@mail.com', 25, 'Mujer');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_respuesta`
--

CREATE TABLE `usuario_respuesta` (
  `id_usuario_respuesta` int(10) NOT NULL,
  `id_usuario` int(10) NOT NULL,
  `numero_pregunta` varchar(30) NOT NULL,
  `usuario_respuesta` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario_respuesta`
--

INSERT INTO `usuario_respuesta` (`id_usuario_respuesta`, `id_usuario`, `numero_pregunta`, `usuario_respuesta`) VALUES
(1, 1, 'pregunta 1', 'a'),
(2, 1, 'pregunta 2', 'a'),
(3, 1, 'pregunta 3', 'a'),
(4, 1, 'pregunta 4', 'c'),
(5, 1, 'pregunta 5', 'b'),
(6, 1, 'pregunta 6', 'b'),
(7, 1, 'pregunta 7', 'a'),
(8, 1, 'pregunta 8', 'c'),
(9, 1, 'pregunta 9', 'a'),
(10, 1, 'pregunta 10', 'a'),
(11, 2, 'pregunta 1', 'b'),
(12, 2, 'pregunta 2', 'a'),
(13, 2, 'pregunta 3', 'b'),
(14, 2, 'pregunta 4', 'b'),
(15, 2, 'pregunta 5', 'c'),
(16, 2, 'pregunta 6', 'c'),
(17, 2, 'pregunta 7', 'a'),
(18, 2, 'pregunta 8', 'c'),
(19, 2, 'pregunta 9', 'c'),
(20, 2, 'pregunta 10', 'c'),
(21, 3, 'pregunta 1', 'c'),
(22, 3, 'pregunta 2', 'b'),
(23, 3, 'pregunta 3', 'b'),
(24, 3, 'pregunta 4', 'c'),
(25, 3, 'pregunta 5', 'a'),
(26, 3, 'pregunta 6', 'b'),
(27, 3, 'pregunta 7', 'a'),
(28, 3, 'pregunta 8', 'a'),
(29, 3, 'pregunta 9', 'c'),
(30, 3, 'pregunta 10', 'a'),
(31, 4, 'pregunta 1', 'b'),
(32, 4, 'pregunta 2', 'a'),
(33, 4, 'pregunta 3', 'c'),
(34, 4, 'pregunta 4', 'a'),
(35, 4, 'pregunta 5', 'b'),
(36, 4, 'pregunta 6', 'c'),
(37, 4, 'pregunta 7', 'c'),
(38, 4, 'pregunta 8', 'c'),
(39, 4, 'pregunta 9', 'b'),
(40, 4, 'pregunta 10', 'a'),
(41, 5, 'pregunta 1', 'b'),
(42, 5, 'pregunta 2', 'b'),
(43, 5, 'pregunta 3', 'c'),
(44, 5, 'pregunta 4', 'c'),
(45, 5, 'pregunta 5', 'b'),
(46, 5, 'pregunta 6', 'a'),
(47, 5, 'pregunta 7', 'b'),
(48, 5, 'pregunta 8', 'b'),
(49, 5, 'pregunta 9', 'a'),
(50, 5, 'pregunta 10', 'a'),
(51, 6, 'pregunta 1', 'c'),
(52, 6, 'pregunta 2', 'c'),
(53, 6, 'pregunta 3', 'c'),
(54, 6, 'pregunta 4', 'a'),
(55, 6, 'pregunta 5', 'b'),
(56, 6, 'pregunta 6', 'a'),
(57, 6, 'pregunta 7', 'a'),
(58, 6, 'pregunta 8', 'c'),
(59, 6, 'pregunta 9', 'a'),
(60, 6, 'pregunta 10', 'b'),
(61, 7, 'pregunta 1', 'b'),
(62, 7, 'pregunta 2', 'a'),
(63, 7, 'pregunta 3', 'b'),
(64, 7, 'pregunta 4', 'c'),
(65, 7, 'pregunta 5', 'b'),
(66, 7, 'pregunta 6', 'b'),
(67, 7, 'pregunta 7', 'a'),
(68, 7, 'pregunta 8', 'c'),
(69, 7, 'pregunta 9', 'a'),
(70, 7, 'pregunta 10', 'a'),
(71, 8, 'pregunta 1', 'c'),
(72, 8, 'pregunta 2', 'c'),
(73, 8, 'pregunta 3', 'b'),
(74, 8, 'pregunta 4', 'c'),
(75, 8, 'pregunta 5', 'b'),
(76, 8, 'pregunta 6', 'b'),
(77, 8, 'pregunta 7', 'a'),
(78, 8, 'pregunta 8', 'c'),
(79, 8, 'pregunta 9', 'a'),
(80, 8, 'pregunta 10', 'c'),
(81, 9, 'pregunta 1', 'c'),
(82, 9, 'pregunta 2', 'a'),
(83, 9, 'pregunta 3', 'b'),
(84, 9, 'pregunta 4', 'c'),
(85, 9, 'pregunta 5', 'c'),
(86, 9, 'pregunta 6', 'b'),
(87, 9, 'pregunta 7', 'a'),
(88, 9, 'pregunta 8', 'a'),
(89, 9, 'pregunta 9', 'b'),
(90, 9, 'pregunta 10', 'a'),
(91, 10, 'pregunta 1', 'b'),
(92, 10, 'pregunta 2', 'a'),
(93, 10, 'pregunta 3', 'b'),
(94, 10, 'pregunta 4', 'c'),
(95, 10, 'pregunta 5', 'b'),
(96, 10, 'pregunta 6', 'b'),
(97, 10, 'pregunta 7', 'a'),
(98, 10, 'pregunta 8', 'c'),
(99, 10, 'pregunta 9', 'a'),
(100, 10, 'pregunta 10', 'a'),
(101, 11, 'pregunta 1', 'a'),
(102, 11, 'pregunta 2', 'a'),
(103, 11, 'pregunta 3', 'c'),
(104, 11, 'pregunta 4', 'c'),
(105, 11, 'pregunta 5', 'b'),
(106, 11, 'pregunta 6', 'c'),
(107, 11, 'pregunta 7', 'a'),
(108, 11, 'pregunta 8', 'c'),
(109, 11, 'pregunta 9', 'a'),
(110, 11, 'pregunta 10', 'a'),
(111, 12, 'pregunta 1', 'b'),
(112, 12, 'pregunta 2', 'a'),
(113, 12, 'pregunta 3', 'b'),
(114, 12, 'pregunta 4', 'c'),
(115, 12, 'pregunta 5', 'b'),
(116, 12, 'pregunta 6', 'b'),
(117, 12, 'pregunta 7', 'a'),
(118, 12, 'pregunta 8', 'b'),
(119, 12, 'pregunta 9', 'a'),
(120, 12, 'pregunta 10', 'c'),
(121, 13, 'pregunta 1', 'a'),
(122, 13, 'pregunta 2', 'b'),
(123, 13, 'pregunta 3', 'b'),
(124, 13, 'pregunta 4', 'c'),
(125, 13, 'pregunta 5', 'a'),
(126, 13, 'pregunta 6', 'b'),
(127, 13, 'pregunta 7', 'c'),
(128, 13, 'pregunta 8', 'a'),
(129, 13, 'pregunta 9', 'b'),
(130, 13, 'pregunta 10', 'c'),
(131, 14, 'pregunta 1', 'c'),
(132, 14, 'pregunta 2', 'a'),
(133, 14, 'pregunta 3', 'b'),
(134, 14, 'pregunta 4', 'b'),
(135, 14, 'pregunta 5', 'b'),
(136, 14, 'pregunta 6', 'c'),
(137, 14, 'pregunta 7', 'c'),
(138, 14, 'pregunta 8', 'b'),
(139, 14, 'pregunta 9', 'a'),
(140, 14, 'pregunta 10', 'a'),
(141, 15, 'pregunta 1', 'b'),
(142, 15, 'pregunta 2', 'a'),
(143, 15, 'pregunta 3', 'a'),
(144, 15, 'pregunta 4', 'b'),
(145, 15, 'pregunta 5', 'c'),
(146, 15, 'pregunta 6', 'b'),
(147, 15, 'pregunta 7', 'a'),
(148, 15, 'pregunta 8', 'c'),
(149, 15, 'pregunta 9', 'a'),
(150, 15, 'pregunta 10', 'a'),
(151, 16, 'pregunta 1', 'c'),
(152, 16, 'pregunta 2', 'b'),
(153, 16, 'pregunta 3', 'a'),
(154, 16, 'pregunta 4', 'c'),
(155, 16, 'pregunta 5', 'a'),
(156, 16, 'pregunta 6', 'c'),
(157, 16, 'pregunta 7', 'a'),
(158, 16, 'pregunta 8', 'a'),
(159, 16, 'pregunta 9', 'c'),
(160, 16, 'pregunta 10', 'a'),
(161, 17, 'pregunta 1', 'a'),
(162, 17, 'pregunta 2', 'b'),
(163, 17, 'pregunta 3', 'c'),
(164, 17, 'pregunta 4', 'a'),
(165, 17, 'pregunta 5', 'c'),
(166, 17, 'pregunta 6', 'a'),
(167, 17, 'pregunta 7', 'c'),
(168, 17, 'pregunta 8', 'c'),
(169, 17, 'pregunta 9', 'a'),
(170, 17, 'pregunta 10', 'a'),
(171, 18, 'pregunta 1', 'b'),
(172, 18, 'pregunta 2', 'c'),
(173, 18, 'pregunta 3', 'c'),
(174, 18, 'pregunta 4', 'a'),
(175, 18, 'pregunta 5', 'a'),
(176, 18, 'pregunta 6', 'c'),
(177, 18, 'pregunta 7', 'b'),
(178, 18, 'pregunta 8', 'a'),
(179, 18, 'pregunta 9', 'a'),
(180, 18, 'pregunta 10', 'a'),
(181, 19, 'pregunta 1', 'b'),
(182, 19, 'pregunta 2', 'a'),
(183, 19, 'pregunta 3', 'a'),
(184, 19, 'pregunta 4', 'a'),
(185, 19, 'pregunta 5', 'c'),
(186, 19, 'pregunta 6', 'c'),
(187, 19, 'pregunta 7', 'a'),
(188, 19, 'pregunta 8', 'a'),
(189, 19, 'pregunta 9', 'b'),
(190, 19, 'pregunta 10', 'a'),
(191, 20, 'pregunta 1', 'a'),
(192, 20, 'pregunta 2', 'a'),
(193, 20, 'pregunta 3', 'c'),
(194, 20, 'pregunta 4', 'c'),
(195, 20, 'pregunta 5', 'c'),
(196, 20, 'pregunta 6', 'c'),
(197, 20, 'pregunta 7', 'a'),
(198, 20, 'pregunta 8', 'c'),
(199, 20, 'pregunta 9', 'a'),
(200, 20, 'pregunta 10', 'a'),
(201, 21, 'pregunta 1', 'b'),
(202, 21, 'pregunta 2', 'a'),
(203, 21, 'pregunta 3', 'c'),
(204, 21, 'pregunta 4', 'c'),
(205, 21, 'pregunta 5', 'c'),
(206, 21, 'pregunta 6', 'a'),
(207, 21, 'pregunta 7', 'c'),
(208, 21, 'pregunta 8', 'b'),
(209, 21, 'pregunta 9', 'a'),
(210, 21, 'pregunta 10', 'a'),
(211, 22, 'pregunta 1', 'b'),
(212, 22, 'pregunta 2', 'c'),
(213, 22, 'pregunta 3', 'b'),
(214, 22, 'pregunta 4', 'c'),
(215, 22, 'pregunta 5', 'b'),
(216, 22, 'pregunta 6', 'b'),
(217, 22, 'pregunta 7', 'a'),
(218, 22, 'pregunta 8', 'b'),
(219, 22, 'pregunta 9', 'a'),
(220, 22, 'pregunta 10', 'a'),
(221, 23, 'pregunta 1', 'a'),
(222, 23, 'pregunta 2', 'c'),
(223, 23, 'pregunta 3', 'c'),
(224, 23, 'pregunta 4', 'b'),
(225, 23, 'pregunta 5', 'c'),
(226, 23, 'pregunta 6', 'c'),
(227, 23, 'pregunta 7', 'a'),
(228, 23, 'pregunta 8', 'a'),
(229, 23, 'pregunta 9', 'a'),
(230, 23, 'pregunta 10', 'a'),
(231, 24, 'pregunta 1', 'b'),
(232, 24, 'pregunta 2', 'a'),
(233, 24, 'pregunta 3', 'c'),
(234, 24, 'pregunta 4', 'b'),
(235, 24, 'pregunta 5', 'a'),
(236, 24, 'pregunta 6', 'c'),
(237, 24, 'pregunta 7', 'a'),
(238, 24, 'pregunta 8', 'b'),
(239, 24, 'pregunta 9', 'a'),
(240, 24, 'pregunta 10', 'a'),
(241, 25, 'pregunta 1', 'c'),
(242, 25, 'pregunta 2', 'a'),
(243, 25, 'pregunta 3', 'a'),
(244, 25, 'pregunta 4', 'a'),
(245, 25, 'pregunta 5', 'a'),
(246, 25, 'pregunta 6', 'b'),
(247, 25, 'pregunta 7', 'a'),
(248, 25, 'pregunta 8', 'c'),
(249, 25, 'pregunta 9', 'a'),
(250, 25, 'pregunta 10', 'a'),
(251, 26, 'pregunta 1', 'c'),
(252, 26, 'pregunta 2', 'a'),
(253, 26, 'pregunta 3', 'b'),
(254, 26, 'pregunta 4', 'c'),
(255, 26, 'pregunta 5', 'b'),
(256, 26, 'pregunta 6', 'b'),
(257, 26, 'pregunta 7', 'b'),
(258, 26, 'pregunta 8', 'b'),
(259, 26, 'pregunta 9', 'b'),
(260, 26, 'pregunta 10', 'a'),
(261, 27, 'pregunta 1', 'a'),
(262, 27, 'pregunta 2', 'c'),
(263, 27, 'pregunta 3', 'b'),
(264, 27, 'pregunta 4', 'b'),
(265, 27, 'pregunta 5', 'b'),
(266, 27, 'pregunta 6', 'c'),
(267, 27, 'pregunta 7', 'a'),
(268, 27, 'pregunta 8', 'c'),
(269, 27, 'pregunta 9', 'a'),
(270, 27, 'pregunta 10', 'a'),
(271, 28, 'pregunta 1', 'b'),
(272, 28, 'pregunta 2', 'a'),
(273, 28, 'pregunta 3', 'b'),
(274, 28, 'pregunta 4', 'c'),
(275, 28, 'pregunta 5', 'b'),
(276, 28, 'pregunta 6', 'a'),
(277, 28, 'pregunta 7', 'a'),
(278, 28, 'pregunta 8', 'c'),
(279, 28, 'pregunta 9', 'a'),
(280, 28, 'pregunta 10', 'a');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_test`
--

CREATE TABLE `usuario_test` (
  `id_usuario_pregunta` int(10) NOT NULL,
  `id_usuario` int(10) NOT NULL,
  `genero` varchar(15) NOT NULL,
  `dia` int(10) NOT NULL,
  `numero_respuestas_correctas` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario_test`
--

INSERT INTO `usuario_test` (`id_usuario_pregunta`, `id_usuario`, `genero`, `dia`, `numero_respuestas_correctas`) VALUES
(1, 1, 'Mujer', 1, 8),
(2, 2, 'Hombre', 2, 5),
(3, 3, 'Mujer', 3, 5),
(4, 4, 'Hombre', 4, 5),
(5, 5, 'Hombre', 4, 5),
(6, 6, 'Hombre', 6, 4),
(7, 7, 'Mujer', 7, 10),
(8, 8, 'Hombre', 8, 7),
(9, 9, 'Mujer', 9, 6),
(10, 10, 'Hombre', 10, 10),
(11, 11, 'Mujer', 11, 7),
(12, 12, 'Hombre', 12, 8),
(13, 13, 'Mujer', 12, 3),
(14, 14, 'Hombre', 14, 5),
(15, 15, 'Mujer', 15, 7),
(16, 16, 'Hombre', 16, 3),
(17, 17, 'Mujer', 17, 3),
(18, 18, 'Hombre', 18, 3),
(19, 19, 'Mujer', 19, 4),
(20, 20, 'Hombre', 20, 6),
(21, 21, 'Mujer', 21, 5),
(22, 22, 'Hombre', 22, 8),
(23, 23, 'Mujer', 23, 3),
(24, 24, 'Hombre', 24, 5),
(25, 25, 'Mujer', 25, 6),
(26, 26, 'Hombre', 26, 6),
(27, 27, 'Hombre', 26, 6),
(28, 28, 'Mujer', 28, 9);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- Indices de la tabla `usuario_respuesta`
--
ALTER TABLE `usuario_respuesta`
  ADD PRIMARY KEY (`id_usuario_respuesta`),
  ADD KEY `id_usuario` (`id_usuario`);

--
-- Indices de la tabla `usuario_test`
--
ALTER TABLE `usuario_test`
  ADD PRIMARY KEY (`id_usuario_pregunta`),
  ADD KEY `id_usuario_respuesta` (`id_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT de la tabla `usuario_respuesta`
--
ALTER TABLE `usuario_respuesta`
  MODIFY `id_usuario_respuesta` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=281;

--
-- AUTO_INCREMENT de la tabla `usuario_test`
--
ALTER TABLE `usuario_test`
  MODIFY `id_usuario_pregunta` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `usuario_respuesta`
--
ALTER TABLE `usuario_respuesta`
  ADD CONSTRAINT `usuario_respuesta_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `usuario_test`
--
ALTER TABLE `usuario_test`
  ADD CONSTRAINT `usuario_test_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
